# Declaring Characters

# in this game, "a" refers to the main character (Lottie Shinju) and "b" refers to the enemy (salamander HQ)... etc
define a = Character("Lottie Shinju")
define b = Character("enemy")
define c = Character("Injured Lad")

# NVL characters are used for the phone texting
define a_nvl = Character("Lottie", kind=nvl, callback=Phone_SendSound)
define b_nvl = Character("???", kind=nvl, callback=Phone_ReceiveSound)

# ignore this...
define config.adv_nvl_transition = None
define config.nvl_adv_transition = Dissolve(0.3)

# resizing the sprites to fit on the screen 
image lottie_neutral_left = im.FactorScale("lottie_neutral_left.png", 0.52)
image lottie_neutral_right = im.FactorScale("lottie_neutral_right.png", 0.52)

image lottie_gross_left = im.FactorScale("lottie_gross_left.png", 0.52)
image lottie_gross_right = im.FactorScale("lottie_gross_right.png", 0.52)

image lottielads = im.FactorScale("lottielads.png", 0.3)


# ignore this...
transform midright:
    xalign 0.8
    yalign 1.0

label start:
    # START OF ACT 1

    # load the screen "still_ocean_bg.jpg", with fade is the transition style so it fades in
    scene still_ocean_bg
    with fade

    # !!!putting text like this is how to make the narrator say something!!!
    "Holiday. A special time of the year that brings folks together in celebration"
    "But there exists a day above all others where an Axolotl's silliness potential reaches its peak"

    "Today is that day..."


    "Lottie's Birthday!"

    "Our favorite axolotl wakes up to a bright and sunny day!"

    # playing the transition video
    play movie "./images/bubble_transition.webm"

    # changing the scene while the movie plays so it doesn't look glitchy
    scene still_room_bg
    # pause for 3 seconds (length of the transition), hard=True means she can't skip the cutscene
    $ renpy.pause(3, hard=True)

    #playing bgm
    play music "bgm.mp3"

    # appear from right, have to play with x position and the y position to make the character placement good
    show lottie_neutral_left at center with moveinright:
        xalign 0.7 ypos 1640

    # at the top of the script, "a" is defined as "Lottie Shinju", so her name will appear in the text box indicating she's talking
    a "WOOPYO!"
    a "It's finally my birthday!"
    a "I can't wait to party with the girls and the lads!"
    a "This is gonna be the best day ever!"

    # moving her farther to the right so she wont be covered by the phone
    show lottie_neutral_left at center with moveinleft:
        xalign 0.9 ypos 1640


    #Phone conversation start

    # basically a_nvl means that lottie is texting. this was defined at the top of the script.
    nvl_narrator "PixelStart+ Group Chat"
    a_nvl "omigosh hi guys!!"
    a_nvl "guess what day it is today!!!"
    a_nvl "hello?"
    a_nvl "hellooooo?"
    a_nvl "guys?"
    a_nvl "hellooooooo?"
    a_nvl "{image=phone_selfie.jpg}"

    # move lottie somewhere else on the screen
    show lottie_neutral_left:
        ease 0.5 xalign 0.5 

    # lottie talking
    a "What's happening?"
    a "They're usually the first to message in the morning!"
    a "Where is everyone?"

    # narrator talking
    "..."

    # lottie talking
    a "Oh wait!"
    a "They must be trying to surprise me!"
    "Lottie excitedly runs outside"

    #runs left off the screen
    show lottie_neutral_left at offscreenleft with moveinleft:
        ypos 1640

    # fade to black.png
    scene black
    with fade

    # narrator speaking
    "She was not ready for the scene that laid before her..."

    # pause for one second, hard=True means she's unable to skip (can't click to continue for 1 second)
    $ renpy.pause(1, hard=True)

    # END OF ACT 1




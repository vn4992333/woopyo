﻿# The game script

# Declaring Characters
# in this game, a refers to the main character (Lottie) and b refers to the enemy (salamander HQ)
define a = Character("Lottie Shinju")
define b = Character("???")
define c = Character("Jim")
define d = Character("Injured Lad")
define e = Character("Nebu Nebi")
define f = Character("Muina Ukaso")
define g = Character("Rakura Kazuno")
define h = Character("Chalky Katsoomy")
define i = Character("Salamanghidora")
define j = Character("Nebi Nebu")
define k = Character("Miuna Usako")
define l = Character("Chiaki Katsumi")
define m = Character("Raki Kazuki")

# NVL characters are used for the phone texting
define a_nvl = Character("Lottie", kind=nvl, callback=Phone_SendSound)
define b_nvl = Character("???", kind=nvl, callback=Phone_TalkSound)

define config.adv_nvl_transition = None
define config.nvl_adv_transition = Dissolve(0.3)


image lottie_neutral_left = im.FactorScale("lottie_neutral_left.png", 0.52)
image lottie_neutral_right = im.FactorScale("lottie_neutral_right.png", 0.52)

image lottie_gross_left = im.FactorScale("lottie_gross_left.png", 0.52)
image lottie_gross_right = im.FactorScale("lottie_gross_right.png", 0.52)

image lottie_pout_left = im.FactorScale("lottie_pout_left.png", 0.52)
image lottie_pout_right = im.FactorScale("lottie_pout_right.png", 0.52)

image laddielonglegs = im.FactorScale("LaddieLegs.png", 0.37)

image msnebi = im.FactorScale("msnebi.png", 0.4)
image msmiuna = im.FactorScale("msmiuna.png", 0.7)
image msraki = im.FactorScale("msraki.png", 1.1)
image mschiaki = im.FactorScale("mschiaki.png", 1.1)

image cartoonfight = im.FactorScale("cartoon_fight.png", 1.7)

image nametag = im.FactorScale("nametag.png", 0.32)

define fadehold = Fade(0.5, 0.5, 0.5)

transform midright:
    xalign 0.8
    yalign 1.0

label start:
    #START OF ACT 1

    scene still_ocean_bg
    with fade

    "Holiday. A special time of the year that brings folks together in celebration"
    "But there exists a day above all others where an Axolotl's silliness potential reaches its peak"

    "Today is that day..."


    "Lottie's Birthday!"

    "Our favorite axolotl wakes up to a bright and sunny day!"

    # this is how to play a video playing a video


    play movie "./images/bubble_transition.webm"

    scene still_room_bg
    $ renpy.pause(3, hard=True)

    #playing bgm
    #play music "bgm.mp3"

    # appear from right
    show lottie_neutral_left at center with moveinright:
        xalign 0.7 ypos 1640

    a "WOOPYO!"
    a "It's finally my birthday!"
    a "I can't wait to party with the girls and the lads!"
    a "This is gonna be the best day ever!"


    show lottie_neutral_left at center with moveinleft:
        xalign 0.9 ypos 1640

    #Phone conversation start


    nvl_narrator "PixelStart+ Group Chat"
    a_nvl "omigosh hi guys!!"
    a_nvl "guess what day it is today!!!"
    a_nvl "hello?"
    a_nvl "hellooooo?"
    a_nvl "guys?"
    a_nvl "hellooooooo?"
    a_nvl "{image=phone_selfie.jpg}"

    show lottie_neutral_left:
        ease 0.5 xalign 0.5 

    a "What's happening?"
    a "They're usually the first to message in the morning!"
    a "Where is everyone?"

    "..."

    a "Oh wait!"
    a "They must be trying to surprise me!"
    "Lottie excitedly runs outside"

    #runs left off the screen
    show lottie_neutral_left at offscreenleft with moveinleft:
        ypos 1640

    scene black
    with fade

    "She was not ready for the scene that laid before her..."

    $ renpy.pause(1, hard=True)

    # END OF ACT 1

    # START OF ACT 2

    #scene panning back and forth between a bunch of lads lying down defeated
    #$ renpy.movie_cutscene("./images/example.webm")
    
    stop music
    play movie "./images/defeat_scene.webm"

    $ renpy.pause(10, hard=True)
    
    #turn that scene into a still background
    scene defeat
    with fadehold

    play music "sad_drama_bgm.mp3"

    show lottie_gross_right at left with moveinleft:
        ypos 1640
    
    a "EW"
    a "I mean..."

    show lottie_pout_right at left:
        ypos 1640

    hide lottie_gross_right

    a "what happened to you guys?"

    "Lottie looks around at all of the lads scattered across the ocean floor"

    a "wait..."
    a "where are your guys' axolotl sandals?"

    "A heavily injured lad limps to lottie"


    scene defeat_noleg
    with None

    show lottie_pout_right at left with None:
        ypos 1640

    show laddielonglegs at midright:
        yalign 0.9
    with moveinright


    c "We tried to stop them Lottie..."
    c "they..."
    c "they took our axolotl sandals..."
    c "and forced us to wear salamander shoes..."

    c "I... I couldn't protect everyone..."
    c "they... they are-"
    "Jim collapses from exhaustion"

    hide laddielonglegs
    with dissolve

    a "WHO? WHO ARE THEY?"

    hide lottie_pout_right with dissolve

    "Lottie rushes back into her home to grab a big box"

    show lottie_pout_left at right with moveinright:
        ypos 1640

    a "Here! All of my spare axolotl sandals! Put them on…"
    
    "As Lottie tries to make sense of whats happening, she receives an ominous message..."

    #clears the phone screen
    nvl clear

    #notification sound

    #Phone conversation start


    nvl_narrator "???"
    b_nvl "{image=voice_message_icon.png}"

    a "..."
    a "WHAT?"
    a "I have to go rescue the girls..."

    "Lottie rushes out of sight"
    show lottie_pout_left at offscreenleft with moveinleft:
        ypos 1640
    
    
    d "She's gonna need all the help she can get…"

    $ renpy.pause(0.3) 
    
    scene black
    with fade

    # END OF ACT 2

    # START OF ACT 3

    "And so our young Lotl set off in search of clues"

    stop music
    
    scene forest_bg
    with fade

    play music forest_bgm

    show lottie_neutral_right at left with moveinleft:
        ypos 1640

    "A suspicious group approaches as she reaches land"
    "They seem… familiar"

    a "What the..."

    show msnebi at right with moveinright:
        xalign 0.5 yalign 0.8
    show msmiuna behind msnebi at right with moveinright:
        ypos 1000
        xalign 0.7
    show msraki at right with moveinright:
        xalign 0.87 yalign 0.8
    show mschiaki behind msraki at right with moveinright:
        xalign 1.0 yalign 0.8


    a "HUUUUUH???"
    a "WHO ARE YOU SUPPOSED TO BE?!"
    e "Must..."
    f "Replace..."
    g "Lottie..."
    h "Shinju..."
    e "Replace..."
    f "The..."
    g "PixelLink..."
    h "Girls..."
    e "Then..."
    f "Rule..."
    g "The..."
    h "World..."

    a "NO! YOU CAN'T-"
    "as Lottie tries to reason with them, she gets attacked..."

    e "Siliness..."
    f "Will..."
    g "Never..."
    h "Prevail..."

    play sound "goofypunch.mp3"
    show msnebi at right with moveinright:
        xalign 0.4 yalign 0.8
    show msnebi at right with moveinright:
        xalign 0.5 yalign 0.8
    play sound "goofypunch.mp3"
    show msmiuna behind msnebi at right with moveinright:
        ypos 1000
        xalign 0.4
    show msmiuna behind msnebi at right with moveinright:
        ypos 1000
        xalign 0.7
    play sound "goofypunch.mp3"
    show msraki at right with moveinright:
        xalign 0.4 yalign 0.8
    show msraki at right with moveinright:
        xalign 0.87 yalign 0.8
    play sound "goofypunch.mp3"
    show mschiaki behind msraki at right with moveinright:
        xalign 0.4 yalign 0.8
    show mschiaki behind msraki at right with moveinright:
        xalign 1.0 yalign 0.8

    a "hey! stop that!"

    
    
    "Lottie retaliates with 4 swift tailwhips"

    # have to redefine them as behind lottie so lottie's sprite is on top of them when she attacks
    
    show msnebi at right behind lottie_neutral_right:
        xalign 0.5 yalign 0.8
    show msmiuna behind msnebi, lottie_neutral_right at right:
        ypos 1000
        xalign 0.7
    show msraki at right behind lottie_neutral_right:
        xalign 0.87 yalign 0.8
    show mschiaki behind msraki, lottie_neutral_right at right:
        xalign 1.0 yalign 0.8

    

    show lottie_neutral_right at right with moveinleft:
        ypos 1640
    hide lottie_neutral_right with dissolve

    show cartoonfight at right with Dissolve(0.3):
        xpos 2500 ypos 1400
    
    
    hide msraki
    hide msmiuna
    hide mschiaki
    hide msnebi

    play sound "tailwhip_sfx.mp3"

    a "Woopyo! Take this!"

    hide cartoonfight with dissolve

    "Lottie knocks out the attackers"

    show lottie_neutral_right at left with dissolve:
        ypos 1640

    a "..."
    a "YOU!"
    a "Where are my genmates!? WHO'S BEHIND THIS?!"

    "Lottie takes a closer look at the attackers"
    a "..."
    a "Wait a minute! This is a costume!"
    "Lottie tears off the costume"

    # show corporate shark
    a "SHARKS?"
    "Lottie takes a closer look, finding a business card lying on top of one of the sharks"

    show nametag at center with dissolve:
        ypos 0.8

    a "That's the Salamacdonalds logo..."
    a "I should have known they'd be behind this!"
    a "I don't have time to deal with small fry, the girls need me!"

    hide nametag

    show lottie_neutral_right at offscreenright with moveinright:
        ypos 1640
    "And so our pink protagonist rushes headstrong towards Salamacdonalds HQ, taking down any goon foolish enough to stand in her way"

    scene black
    with fade

    stop music
    play movie "./images/confrontation.webm"
    $ renpy.pause(42, hard=True)
    
    scene black
    with fade

    "A loud chant could be heard in the distance..."

    play movie "./images/woopyochant.webm"
    $ renpy.pause(17,hard=True)

    scene mascots_bg
    with fade

    play music heroic_bgm

    a "All the Lads are here from Hidden Lotl Village! EW- I mean thanks for coming guys!"

    a "Omigosh look! It's the Miunnies from the Delulu Bread Village!"

    a "And wow! That's the Katsudons of the Geeky Gamer Village!"

    a "Oh and there's the Trashpans of the Stumbling Raccoon Village!"

    a "And the Neblings from the Scuffed Alien Village! Wait what does that symbol even mean?"

    "Nebi was too broke to affor- ahem... too under the influence of the salamanders to respond to that question, however it appeared to partially break some of the control that the salamanders had upon the alien and she reached out towards Lottie"

    stop music

    scene all_girls_glow
    with fade

    play music bells

    a "Nebi! What happened?"

    j "They said they'd talk to the IRS and get me a tax break on all these shadowless pokemon booster packs… "
    j "I'm sorry Lottie..."
    j "I spent all my remaining money on two cheeseburgers with no patties…"
    j "I'll have to sell the spaceship at this point…"
    a "That's just silly Nebi!" 
    j "The IRS would never give a human a tax break on Pokemon cards, let alone an alien invader!"

    stop music

    play movie "./images/nebi_flashback.webm"
    $ renpy.pause(15, hard=True)

    scene 3_girls_glow

    "The Aliens tiktok infused brain was trying to make sense of the information she had just received"


    "Nebi Nebu quickly snaps out of the Salamanders control"

    a "Nebi! Welcome back!"

    play music bells 
    a "Miuna!"

    k "If I defeat them... you can get me my (B)Ryan right?"

    i "With my technologically advanced salamander shoes, I can get you a whole army of (B)Ryans to do your every bidding!"

    a "He's lying to you Miu! You're looking more like a discord mod by the second, there's no way any real (B)Ryan would want that!"

    k "..."

    a "Also aren't you missing out on grinding those Warframe thingies?!"

    k "OH SHIT!"

    stop music
    play movie "./images/miuna_flashback.webm"
    $ renpy.pause(14, hard=True)

    scene 2_girls_glow
    with dissolve

    "Miuna's degenerate and sleep-deprived gamer soul violently shook as she was freed from the Salamanders control"


    play music bells 

    a "What did he promise you Chiaki?"

    l "He promised me he could get me information on the new Tomodachi Life…"
    l "I need to know if it's in production, Lottie…"
    l "It means more to me than you could ever know…"

    "Chiaki was currently huffing absurd amounts of copium"

    a "Chiaki… You're the world's greatest hacker..."
    a "If there was information out there about the game, don't you think you would have been able to find it yourself?!"

    l "But… There has to be a new Tomodachi Life… There just has to be!"

    a "The game doesn't exist Chiaki! Snap out of it!"

    stop music

    play movie "./images/chiaki_flashback.webm"
    $ renpy.pause(10, hard=True)


    scene one_girls_glow
    with dissolve

    "Lottie slaps the copium mask off of Chiaki's face and she realizes she would in fact have heard of Tomodachi Life 2 already if it did exist (she data-mines the entire internet daily for it)."

    "The Salamanders control over her fades"


    play music bells 

    a "Uhhh Raki, you good?"

    m "W-wuh... Oh hey Lottie… You made it to the p-party, t-thank goodness"

    "Raki looks dazedly over at the other girls"

    m "H-hey girls l-look, Lottie's here…"

    a "This isn't a party Raki, and aren't you here because the Salamanders took control of you or something?"

    m "uh… S-salamanders? N-nah I was just f-following the g-girls to your party and we s-stopped at the konbini to get snacks…"
    m "T-they had this really tasty looking w-watermelon… B-but I couldn't wait so…"

    a "Omigosh Raki…"

    stop music 
    play movie "./images/raki_flashback.webm"
    $ renpy.pause(6, hard=True)

    scene no_girls_glow_thumbsup
    with dissolve

    "Raki begins to try and incoherently explain herself but all that comes out is beautiful rainbow sparkles as the 69 cent watermelon continues to take its natural course through her Raccoon body"

    a "Umm… Nevermind"

    play movie "./images/finale.webm"
    $ renpy.pause(143, hard=True)













    "the end placeholder text"

    return

